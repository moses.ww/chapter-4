//Luas Lingkaran

function hitungLuasLingkaran() {
    const phi = 3.14;
    let jariJari = 7;
    const luas = phi * jariJari * jariJari;
    return luas;
  }


//Keliling Lingkaran
  function hitungKelilingLingkaran() {
    const phi = 3.14;
    let jariJari = 7;
    const keliling = 2 * phi * jariJari;
    return keliling;
  }

const luas = hitungLuasLingkaran();
const keliling = hitungKelilingLingkaran();

console.log("Luas lingkaran " + luas);
console.log("Keliling lingkaran " + keliling);